# Oath

> An open source healthcare information system

__Oath__ strives to be a type-safe, memory-safe, secure, privacy-driven healthcare information system that is highly customizable.
